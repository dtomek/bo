import matplotlib
from algorithm import computation
from data import Config
matplotlib.use('TkAgg')
import os
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from ttk import Frame, Entry, Button, LabelFrame, Label, Combobox, Radiobutton, Checkbutton
import tkMessageBox
from Tkinter import Tk, W, E, N, S, BOTH, IntVar

config = Config("a280.tsp", 100, 200)
which_alg = 1
l = None
map_of_widgets = {}
with_heur = True

def create_authors_frame(root):
    def sel():
        global which_alg
        which_alg = var.get()
    def sel1():
        global with_heur
        with_heur = var1.get()
    var = IntVar()
    var1 = IntVar()
    frame = LabelFrame(root, text='authors')
    frame.columnconfigure(0, weight=1, pad=3)
    frame.rowconfigure(0, weight=1, pad=3)
    frame.rowconfigure(1, weight=1, pad=3)
    frame.rowconfigure(2, weight=1, pad=3)
    frame.rowconfigure(3, weight=1, pad=3)

    R1 = Radiobutton(frame, text='Inverse Mutation', value = 1, command=sel, variable=var)
    R1.pack(anchor = W)
    R2 = Radiobutton(frame, text='PMX', value = 2, command=sel, variable=var)
    R2.pack(anchor = W)
    R3 = Radiobutton(frame, text='OX', value = 3, command=sel, variable=var)
    R3.pack(anchor = W)
    R4 = Radiobutton(frame, text='Swap', value = 4, command=sel, variable=var)
    R4.pack(anchor = W)

    CB = Checkbutton(frame, text='Heuristics: ', command=sel1, variable=var1)
    CB.pack(anchor = W)

    return frame


def create_TSP_radiobuttons_frame(root):
    frame = LabelFrame(root, text='TSP file')
    frame.columnconfigure(0, weight=1, pad=3)
    frame.rowconfigure(0, weight=2, pad=3)

    def TSP_selection(event):
        global config
        config.gridfromname(c.get())
    tsp = IntVar()
    c = Combobox(frame, state = "readonly", values=filter(lambda x: x.endswith(".tsp"), os.listdir(".")))
    c.bind("<<ComboboxSelected>>", TSP_selection)
    c.grid(row=0, column=0)
    label = Label(frame)
    label.config(text = '  No selection yet  ')
    label.grid(row=3, column=0)

    return frame


def create_sliders_frame(root):
    global map_of_widgets
    frame = LabelFrame(root, text='Variables')
    frame.columnconfigure(0, weight=1, pad=3)
    frame.rowconfigure(0, weight=1, pad=3)


    l0 = Label(frame, text='iterations')
    l0.grid(row=0, column=0, sticky=W)
    e0 = Entry(frame)
    e0.insert(0, '10')
    e0.grid(row=0, column=0)
    e0.bind()
    map_of_widgets['iterations'] = e0

    l1 = Label(frame, text='permutations')
    l1.bind()
    l1.grid(row=1, column=0, sticky=W)
    e1 = Entry(frame)
    e1.insert(0, '10')
    e1.grid(row=1, column=0)
    map_of_widgets['permutations'] = e1

    return frame


def create_settings_frame(root):
    frame = Frame(root)
    frame.columnconfigure(0, weight=1, pad=3)
    frame.rowconfigure(0, weight=1, pad=3)
    frame.rowconfigure(1, weight=1, pad=3)
    frame.rowconfigure(2, weight=1, pad=3)

    create_TSP_radiobuttons_frame(frame).grid(row=0, column=0, sticky=N+S+E+W)
    create_sliders_frame(frame).grid(row=1, column=0, sticky=N+S+E+W)
    create_authors_frame(frame).grid(row=2, column=0, sticky=N+S+E+W)

    return frame


def create_presentation_frame(root):
    global map_of_widgets
    frame = Frame(root)
    frame.columnconfigure(0, weight=1, pad=3)
    frame.rowconfigure(0, weight=16, pad=3)
    frame.rowconfigure(1, weight=1,  pad=3)

    f = Figure()
    a = f.add_subplot(111)

    a.set_title('Tk embedding')
    a.set_xlabel('X axis label')
    a.set_ylabel('Y label')

    canvas = FigureCanvasTkAgg(f, master=frame)
    canvas.show()
    canvas.get_tk_widget().grid(row=0, column=0, sticky=N+S+E+W)
    # canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)
    map_of_widgets["canvas"] = canvas
    map_of_widgets["plot"] = a
    # listbox = Listbox(frame)
    # for i in range(20):
    #     listbox.insert(END, str(i))
    # listbox.grid(row=0, column=0, sticky=N+S+E+W)

    button = Button(frame, text='START', command=main_algorithm)
    button.grid(row=1, column=0, sticky=N+S+E+W)

    return frame


class Window(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)  # old style class...
        self.parent = parent
        self.parent.wm_title("Firefly Algorithm")
        self.parent.minsize(width=640, height=480)

    def entry(self):
        self.columnconfigure(0, weight=3, pad=3)
        self.columnconfigure(1, weight=1, pad=3)
        self.rowconfigure(0, weight=1)
        
        create_presentation_frame(self).grid(row=0, column=0, sticky=N+S+W+E)
        create_settings_frame(self).grid(row=0, column=1, sticky=N+S+W+E)

        self.pack(fill=BOTH, expand=1)
        
    def show(self):
        self.parent.mainloop()


def main_algorithm():
    global config, map_of_widgets, which_alg, with_heur
    config.num_of_permutations = int(map_of_widgets['permutations'].get())
    config.num_of_steps = int(map_of_widgets['iterations'].get())
    config.whichAlgorithm = which_alg
    config.withHeuristics = with_heur
    print config
    result,val,iter = computation(config, map_of_widgets)
    map_of_widgets["plot"].plot(result)
    map_of_widgets["canvas"].show()

    tkMessageBox.showinfo("Log", "Best value:" + str(val) +"\nIn iteration: " + str(iter))

    print "finish"


def main():
    window = Window(Tk())
    window.entry()
    window.show()

if __name__ == '__main__':
    main()


    # heurystyki poczatkowe
    # mutacje
    # ruch - alhgotyrtm k-zmiany
    # kolejnosc
    # jasnosc