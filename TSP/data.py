from math import sqrt
import re


class Config:
    def __init__(self, name, num_of_steps, num_of_permutations, withHeuristics=True, whichAlgorithm=1):
        self.name = name
        self.adj_matrix = [map(int, re.match("\s*\d*\s*(\d*)\s*(\d*)", line).groups()) for line in open(name)]
        self.num_of_steps = num_of_steps
        self.vertices = len(self.adj_matrix)
        self.num_of_permutations = num_of_permutations
        self.energy_counter = EnergyCounter(self.vertices, self.adj_matrix)
        self.withHeuristics = withHeuristics
        self.whichAlgorithm = whichAlgorithm

    def __str__(self):
         return "File: " +  str(self.name) + "Population: " + str(self.num_of_permutations) + "Steps: " + str(self.num_of_steps)

    def gridfromname(self, name):
        self.name = name
        self.adj_matrix = [map(int, re.match("\s*\d*\s*(\d*)\s*(\d*)", line).groups()) for line in open(name)]
        self.vertices = len(self.adj_matrix)
        self.energy_counter = EnergyCounter(self.vertices, self.adj_matrix)

class EnergyCounter:
    def __init__(self, n, adj_matrix):
        self.n = n
        self.adj_matrix = adj_matrix

    def energy(self, route):
        dist = 0
        for i in xrange(self.n):
            dist += pdist(self.adj_matrix[route[i-1]], self.adj_matrix[route[i]])
        return -dist


def pdist(x, y):
    return sqrt((x[0]-y[0])*(x[0]-y[0]) + (x[1]-y[1])*(x[1]-y[1]))



