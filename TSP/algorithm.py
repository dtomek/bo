from random import sample, shuffle, random, randint
from sys import maxint
from TSP.data import Config



def computation(config, map_of_widgets):
    n = config.vertices
    t = config.num_of_steps
    perms = config.num_of_permutations
    iter_num = 0
    energy = config.energy_counter
    fireflies = [sample(range(n), n) for _ in range(config.num_of_permutations)]
    best_values = []
    if config.withHeuristics:
        fireflies[2] = heuristics(config)
    if config.whichAlgorithm == 1:
        mutate = inverse_rotation
    elif config.whichAlgorithm == 2:
        mutate = pmx_crossover
    elif config.whichAlgorithm == 3:
        mutate = cross
    else :
        mutate = swap
    max_to_log = local_max = -maxint
    inIteration = 0
    while iter_num < t:
        if iter_num % 10 == 0:
            print iter_num
        iter_num += 1
        for i in xrange(perms):
            for j in xrange(perms):
                local_max = max(local_max,  energy.energy(fireflies[i]))
                if energy.energy(fireflies[i]) < energy.energy(fireflies[j]):
                    fireflies[i] = mutate(fireflies[i][:], fireflies[j][:])
        result = map(energy.energy, fireflies)
        if max_to_log < local_max:
            max_to_log = local_max
            inIteration = iter_num
        best_values.append(-local_max)
    map_of_widgets["plot"].plot(best_values, 'b')
    map_of_widgets["canvas"].show()
    return best_values, -max_to_log, inIteration

def heuristics(config):
    def cartdist(x,y):
        return abs(matrix[x][0] - matrix[y][0]) + abs(matrix[y][1]-matrix[x][1])
    n = config.vertices
    matrix = config.energy_counter.adj_matrix
    pool = range(n)
    result = [0]
    pool.remove(0)
    while pool:
        min = pool[0], cartdist(result[-1], pool[0])
        for i in pool:
            tmp = cartdist(result[-1], i)
            if tmp < min[1]:
                min = i, tmp
        result.append(min[0])
        pool.remove(min[0])
    return result



def inverse_rotation(route, neighbour):
    dist = distance(route, neighbour)
    route = neighbour[:]
    rlen = randint(2, dist)
    tmp = randint(0, len(route)-rlen)
    route[tmp:tmp+rlen] = route[tmp:tmp+rlen][::-1]
    return route

def swap(route, neighbour):
    x = randint(0, len(route) - 2)
    route[x], route[x+1] = route[x+1], route[x]
    return route


def pmx_crossover(ind1, ind2):
        size = len(ind1)
        p1, p2 = [0]*size, [0]*size
        for i in xrange(size):
            p1[ind1[i]] = i
            p2[ind2[i]] = i
        # Choose crossover points
        cxpoint1 = randint(0, size)
        cxpoint2 = randint(0, size - 1)
        if cxpoint2 >= cxpoint1:
            cxpoint2 += 1
        else: # Swap the two cx points
            cxpoint1, cxpoint2 = cxpoint2, cxpoint1

        # Apply crossover between cx points
        for i in xrange(cxpoint1, cxpoint2):
            # Keep track of the selected values
            temp1 = ind1[i]
            temp2 = ind2[i]
            # Swap the matched value
            ind1[i], ind1[p1[temp2]] = temp2, temp1
            ind2[i], ind2[p2[temp1]] = temp1, temp2
            # Position bookkeeping
            p1[temp1], p1[temp2] = p1[temp2], p1[temp1]
            p2[temp1], p2[temp2] = p2[temp2], p2[temp1]
        return ind1

def cross(result, parent2):
        l = len(result)
        result =result
        mn,mx = randint(0, l-1), randint(0, l-1)
        mn,mx = min(mn,mx), max(mn,mx)
        slicee = result[mn:mx]
        i,j = 0,0
        while i < l-(mx-mn):
            while parent2[(mx + j)%l] in slicee:
                j+=1
            result[(mx+i)%l] = parent2[(mx + j)%l]
            i+=1
            j+=1
        return result


def distance(p1, p2):
    i = 0
    for x in xrange(len(p2)):
        if p1[x] != p2[x]:
            i += 1
    return i

config = Config("a280.tsp", 10000, 30, whichAlgorithm=3)
if __name__ == '__main__':
    computation(config)

